
var pressQueue = [];
var input = {};
var prevInput = {};

document.addEventListener("keydown", function(e) {
	if (e.key == "ArrowDown") input.down = true;
	if (e.key == "ArrowLeft") input.left = true;
	if (e.key == "ArrowRight") input.right = true;
	if (e.key == "ArrowUp") input.up = true;
});
document.addEventListener("keyup", function(e) {
	if (e.key == "ArrowDown") input.down = false;
	if (e.key == "ArrowLeft") input.left = false;
	if (e.key == "ArrowRight") input.right = false;
	if (e.key == "ArrowUp") input.up = false;
});
window.addEventListener("blur", function (e) {
	input = {};
})

function inputFrame() {

	if (pressQueue.length < 2) {
		if (!prevInput.left && input.left) pressQueue.push({left: true});
		if (!prevInput.right && input.right) pressQueue.push({right: true});
		if (!prevInput.up && input.up) pressQueue.push({up: true});
		if (!prevInput.down && input.down) pressQueue.push({down: true});
	}


	prevInput = Object.assign({}, input);
}

